package com.lilacgames.learning.animation_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button simpleAnimationButton = (Button) findViewById(R.id.simpleAnimationButton);
        simpleAnimationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,SimpleAnimationActivity.class);
            }
        });


        Button propertyAnimButton = (Button) findViewById(R.id.propertyAnimationButton);
        propertyAnimButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,PropertyAnimationActivity.class);
                startActivity(i);
            }
        });
    }
}
